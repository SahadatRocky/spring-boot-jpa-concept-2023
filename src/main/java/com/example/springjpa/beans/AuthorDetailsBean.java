package com.example.springjpa.beans;
import lombok.Data;

@Data
public class AuthorDetailsBean {
    private  Integer Id;
    private String name;
}
