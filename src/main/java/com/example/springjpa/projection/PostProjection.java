package com.example.springjpa.projection;

public interface PostProjection {

    Integer getId();
    String getTitle();
    String getContent();
    String getAuthorName();
}
