package com.example.springjpa.projection;

public interface AuthorProjection {

    Integer getId();
    String getName();
}
