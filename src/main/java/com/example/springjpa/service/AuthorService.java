package com.example.springjpa.service;

import com.example.springjpa.beans.AuthorBean;
import com.example.springjpa.beans.AuthorDetailsBean;
import com.example.springjpa.beans.AuthorListBean;
import com.example.springjpa.beans.DropdownDTO;
import com.example.springjpa.exception.ServiceExceptionHolder;
import com.example.springjpa.models.Author;
import com.example.springjpa.projection.AuthorProjection;
import com.example.springjpa.repository.AuthorRepository;
import com.example.springjpa.util.AppConstant;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AuthorService {

    private AuthorRepository authorRepository;
    private ModelMapper modelMapper;

    private final ObjectMapper objectMapper;

    private final RedisTemplate<String, Object> redisTemplate;
    public AuthorService(AuthorRepository authorRepository,ModelMapper modelMapper, ObjectMapper objectMapper, RedisTemplate<String, Object> redisTemplate){
         this.authorRepository = authorRepository;
         this.modelMapper = modelMapper;
         this.objectMapper = objectMapper;
         this.redisTemplate = redisTemplate;
    }

    public AuthorBean create(AuthorBean authorBean) {
        Author author = modelMapper.map(authorBean, Author.class);
        Author save = authorRepository.save(author);
        return modelMapper.map(save, AuthorBean.class);
    }

    public List<AuthorProjection> getAuthorProjectionList() {
        return authorRepository.findAllAuthor();
    }

    public Page<AuthorListBean> getAll(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("id")));
        Page<Author> page = authorRepository.findAll(pageRequest);
        List<AuthorListBean> beans = page.getContent()
                .stream()
                .sorted(Comparator.comparing(Author::getId).reversed())
                .map(this:: getAuthorInfoListBean).collect(Collectors.toUnmodifiableList());


        String dataKey = DigestUtils.sha256Hex("#DATA#AUTHOR#" + 1 + "#TEST#");
        log.info("redis-data========={}",redisTemplate.opsForValue().get(dataKey));
        return new PageImpl<>(beans, page.getPageable(), page.getTotalElements());
    }

    private AuthorListBean getAuthorInfoListBean(Author author) {
        AuthorListBean bean = modelMapper.map(author, AuthorListBean.class);
        return bean;
    }

    public AuthorDetailsBean getById(Integer id) {
        Optional<Author> optional = authorRepository.findById(id);
        if(optional.isEmpty()){
            return null;
        }

        String dataKey = DigestUtils.sha256Hex("#DATA#AUTHOR#" + id + "#TEST#");
        if (Boolean.TRUE.equals(redisTemplate.hasKey(dataKey))) {
            redisTemplate.delete(dataKey);
        }
        log.info("data========={}",optional);
        JsonNode jsonNode = objectMapper.valueToTree(optional);
        log.info("data jsonNode========={}",jsonNode);
        redisTemplate.opsForValue().set(dataKey, jsonNode, 2, TimeUnit.MINUTES);

        log.info("redis-data========={}",redisTemplate.opsForValue().get(dataKey));

        return getConvertBean(optional.get());
    }

    private AuthorDetailsBean getConvertBean(Author author) {
        AuthorDetailsBean bean = modelMapper.map(author, AuthorDetailsBean.class);
        return bean;
    }

    public List<DropdownDTO> getDropdownlist() {
       List<Author> list = authorRepository.findAll(Sort.by("name")).stream().collect(Collectors.toUnmodifiableList());
       return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(Author author) {
        DropdownDTO dropdownDTO = modelMapper.map(author, DropdownDTO.class);
        dropdownDTO.setValue(author.getName());
        return dropdownDTO;
    }

    public AuthorBean update(Integer id, AuthorBean authorBean) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.EXCEPTION_ID_NOT_FOUND_IN_DB,
                 "Not Found with ID: " + id,
                 "NOt Found with ID: " + id
        ));;
        BeanUtils.copyProperties(authorBean, author, AppConstant.IGNORE_PROPERTIES);
        Author update = authorRepository.save(author);
        return modelMapper.map(update, AuthorBean.class);
    }


    public Object delete(Integer id) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.EXCEPTION_ID_NOT_FOUND_IN_DB,
                "Not Found with ID: " + id,
                "NOt Found with ID: " + id
        ));
        authorRepository.delete(author);
        return Boolean.TRUE;
    }
}
