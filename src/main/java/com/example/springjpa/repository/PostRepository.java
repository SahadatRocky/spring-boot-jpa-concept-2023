package com.example.springjpa.repository;
import com.example.springjpa.models.Post;
import com.example.springjpa.projection.PostProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

    //projection
    @Query("SELECT p.id as id,p.title as title,p.content as content, p.author.name as authorName  FROM Post p")
    List<PostProjection> findAllPost();

    Page<Post> findAll(Pageable pageable);

    Optional<Post> findById(Integer id);

    @Query(value = "SELECT b FROM Post b WHERE lower(b.title) LIKE %:text% OR lower(b.title) LIKE %:text%")
    List<Post> search(String text);

    Page<Post> findByIdIn(List<String> ids, Pageable pageable);

    Page<Post> findAllByIdNotIn(List<String> ids, Pageable pageable);

//    @Query("SELECT ui FROM Post ui WHERE ui.Id NOT IN (:postIds) AND ui.status=1 AND ui.isDeveloper=1 AND ui.isFullAccess=0")
//    Page<Post> findAllDevelopersWithLimitedAccessNotInIds(List<String> postIds, Pageable pageable);

    @Query("SELECT ui FROM Post ui WHERE ui.Id IN (:postIds)")
    Page<Post> findAllInIds(List<String> postIds, PageRequest request);

    @Query("SELECT ui FROM Post ui WHERE ui.Id NOT IN (:list)")
    Page<Post> findAllNotInIds(List<String> list, Pageable pageable);

    @Query(value = "SELECT s FROM Post s WHERE s.id in(:postIds)")
    List<Post> findAllByPostIds(List<Integer> postIds);

    /*

    @Query("""
            SELECT ui.id AS id, ui.userFullName AS fullName, ui.userEmail AS email,\s
            CASE ui.status WHEN 1 THEN 'Active' ELSE 'Inactive' END AS status,\s
            CASE ui.isDeveloper WHEN 1 THEN 'Yes' ELSE 'No' END AS developer,\s
            ut.typeName as typeName\s
            FROM UserInfo ui\s
            LEFT JOIN UserType ut ON ut.id = ui.userType ORDER  BY ui.updatedOn DESC
            """)
    Page<PostProjection> findAllUser(Pageable pageable);


     @Query("""
            SELECT ds.id as id, ds.dashboardId as dashboardId, ds.caption as caption,\s
            ds.status as status, ds.timeline as timeline, CASE WHEN COUNT(dr.id) > 0 THEN TRUE ELSE FALSE END AS hasReport\s
            FROM DashboardUser du\s
            LEFT JOIN UserInfo ui ON ui.id = du.userId OR ui.userType=du.userType\s
            LEFT JOIN Dashboard ds ON ds.id = du.dashboardId\s
            LEFT JOIN DashboardReport dr ON dr.dashboardId = ds.id\s
            WHERE ui.id = :userId AND ds.status=1
            GROUP BY du.dashboardId
            ORDER BY ds.caption, ds.status desc
            """)
    List<UserDashboardInfoProjection> getUserDashboards(@Param("userId") String userId);
    */
}
