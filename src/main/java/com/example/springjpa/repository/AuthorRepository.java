package com.example.springjpa.repository;

import com.example.springjpa.models.Author;
import com.example.springjpa.projection.AuthorProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {

    //projection
    @Query("SELECT a.id as id,a.name as name FROM Author a")
    List<AuthorProjection> findAllAuthor();

    Page<Author> findAll(Pageable pageable);

    Optional<Author> findById(Integer id);


}
