package com.example.springjpa.resource;
import com.example.springjpa.beans.AuthorBean;
import com.example.springjpa.beans.PostBean;
import com.example.springjpa.service.PostService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class PostResource {

    private final PostService postService;

    @GetMapping("/post/get-all")
    public ResponseEntity<?> getAll(Pageable pageable){
        return new ResponseEntity<>(postService.getAll(pageable), HttpStatus.OK);
    }

    @GetMapping("/post/get-by/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Integer id){
        return new ResponseEntity<>(postService.getById(id), HttpStatus.OK);
    }

    @GetMapping("/post/dropdown-list")
    public ResponseEntity<?> getDropdownList(){
        return new ResponseEntity<>(postService.getDropdownlist(), HttpStatus.OK);
    }

    @PostMapping("/post")
    public ResponseEntity<?> create(@RequestBody PostBean postBean){
       return new ResponseEntity<>(postService.create(postBean), HttpStatus.OK);
    }

    @PutMapping("/post/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Integer id, @RequestBody PostBean postBean) {
        return new ResponseEntity<>(postService.update(id, postBean), HttpStatus.OK);
    }

    @GetMapping("/posts")
    public ResponseEntity<?> getPostProjectionList(){
        return new ResponseEntity<>(postService.getPostProjectionList(),HttpStatus.OK);
    }


    @DeleteMapping("/post/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id){
        return new ResponseEntity<>(postService.delete(id), HttpStatus.OK);
    }


    @GetMapping("post/search/{searchText}")
    public ResponseEntity<?> search(@PathVariable("searchText")  String searchText) {
        return new ResponseEntity<>(postService.search(searchText), HttpStatus.OK);
    }


}
